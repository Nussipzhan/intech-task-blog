<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form id="formCreatePost" modelAttribute="createPostForm">
	<table>
		<tr height="50">
			<td>Title </td>
			<td><form:input type="text" path="title"/></td>
			<td style="color: red"><form:errors path="title"/></td>
		</tr>
		<tr height="50">
			<td>Content </td>
			<td><form:textarea path="content" cols="50" rows="10"/></td>
			<td style="color: red"><form:errors path="content"/></td>
		</tr>
		<tr height="50">
			<td></td>
			<td><input type="button" id="btnCreatePost" value="Create post"/></td>
			<td></td>
		</tr>
	</table>	
</form:form>

<script type="text/javascript">
$('#btnCreatePost').click(function () {
	$.post('/blog/createPost.do', $('#formCreatePost').serialize(), function (data) {
		$('#formContainer').html(data);
	});
});
</script>