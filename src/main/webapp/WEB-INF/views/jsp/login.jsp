<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
	<jsp:attribute name="title">
		Login
	</jsp:attribute>
	<jsp:body>
		<div id="formContainer">
			<%@include file="loginForm.jsp" %>
		</div>
		<script type="text/javascript">
			var error = '${error}';
			
			$(document).ready(function () {
				if (error.length > 0) {
					alert(error);		
				}
			});
		</script>
	</jsp:body>
</t:main>

