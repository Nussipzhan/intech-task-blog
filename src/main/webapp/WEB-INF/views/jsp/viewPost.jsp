<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
	<jsp:attribute name="title">
		View post
	</jsp:attribute>
	<jsp:body>
		<h3>${post.title}</h3>
		<div><p>${post.content}</p></div>
		
		<sec:authorize access="hasAnyRole('ADMIN, GUEST')">
			<%@include file="createCommentForm.jsp" %>
		</sec:authorize>
		
		<h3>Comments</h3>
		
		<div id="commentsContainer">
			<c:forEach var="comment" items="${post.comments}">
				<c:set var="comment" value="${comment}" scope="request"/>
				<jsp:include page="commentBlock.jsp"></jsp:include>
			</c:forEach>
		</div>
	</jsp:body>
</t:main>
