<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
	<jsp:attribute name="title">
		Registration
	</jsp:attribute>
	<jsp:body>
		<div id="formContainer">
			<%@include file="regForm.jsp" %>
		</div>
	</jsp:body>
</t:main>

