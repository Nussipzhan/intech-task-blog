<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
	<jsp:attribute name="title">
		Create post
	</jsp:attribute>
	<jsp:body>
		<div id="formContainer">
			<%@include file="createPostForm.jsp" %>
		</div>
	</jsp:body>
</t:main>
