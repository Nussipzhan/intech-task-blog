<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form id="formCreateComment" modelAttribute="createCommentForm">
	<table>
		<tr height="50">
			<td>Comment </td>
			<td><form:textarea path="content" cols="50" rows="10"/></td>
			<td style="color: red"><form:errors path="content"/></td>
		</tr>
		<tr height="50">
			<td></td>
			<td><input type="button" id="btnCreateComment" value="Create comment"/></td>
			<td></td>
		</tr>
	</table>	
</form:form>

<script type="text/javascript">
$('#btnCreateComment').click(function () {
	$.post('/blog/createComment/${post.id}', $('#formCreateComment').serialize(), function (data) {
		$('#commentsContainer').prepend(data);
	});
});
</script>