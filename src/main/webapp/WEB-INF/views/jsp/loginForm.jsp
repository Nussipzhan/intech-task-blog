<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="j_spring_security_check" var="loginUrl" />
<form:form id="formLogin" modelAttribute="loginForm" action="${loginUrl}" method="POST">
	<table>
		<tr height="50">
			<td>Login </td>
			<td><form:input type="text" path="login"/></td>
			<td style="color: red"><form:errors path="login"/></td>
		</tr>
		<tr height="50">
			<td>Password </td>
			<td><form:input type="password" path="password"/></td>
			<td style="color: red"><form:errors path="password"/></td>
		</tr>
		<tr height="50">
			<td></td>
			<td><input type="submit" id="btnLogin" value="Login"/></td>
			<td></td>
		</tr>
	</table>	
</form:form>
