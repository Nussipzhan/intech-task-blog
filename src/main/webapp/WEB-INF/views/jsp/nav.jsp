<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sec:authorize access="isAuthenticated()"> 
<sec:authentication var="user" property="principal" />
Welcome, <b>${user.username}</b>
</sec:authorize>

<ul>
	<li><a href="/blog/">Home</a></li>
	<sec:authorize access="isAnonymous()"> 
		<li><a href="/blog/registration">Registration</a></li>
		<li><a href="/blog/login">Login</a></li>
	</sec:authorize>
	<sec:authorize access="isAuthenticated()">
		<c:url value="j_spring_security_logout" var="logoutUrl" /> 
		<li><a href="javascript:formSubmit()">Logout</a></li>
	</sec:authorize>
</ul>

<form action="${logoutUrl}" method="post" id="logoutForm">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<script>
function formSubmit() {
	document.getElementById("logoutForm").submit();
}
</script>