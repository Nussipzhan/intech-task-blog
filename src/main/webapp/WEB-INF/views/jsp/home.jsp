<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
	<jsp:attribute name="title">
		Posts
	</jsp:attribute>
	<jsp:body>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<a href="/blog/createPost">Create post</a>
		</sec:authorize>
		
		<br>
		
		<c:forEach var="post" items="${posts}">
			<hr width="300" align="left">
			<div>
				<h4><a href="/blog/viewPost/${post.id}">${post.title}</a></h4>
				<div><p>${fn:substring(post.content, 0, 160)}</p></div>
			</div>
		</c:forEach>
	</jsp:body>
</t:main>
