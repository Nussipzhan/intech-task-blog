<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form id="formReg" modelAttribute="regForm">
	<table>
		<tr height="50">
			<td>First name </td>
			<td><form:input type="text" path="firstName"/></td>
			<td style="color: red"><form:errors path="firstName"/></td>
		</tr>
		<tr height="50">
			<td>Last name </td>
			<td><form:input type="text" path="lastName"/></td>
			<td style="color: red"><form:errors path="lastName"/></td>
		</tr>
		<tr height="50">
			<td>Middle name </td>
			<td><form:input type="text" path="middleName"/></td>
			<td style="color: red"><form:errors path="middleName"/></td>
		</tr>
		<tr height="50">
			<td>Email </td>
			<td><form:input type="email" path="email"/></td>
			<td style="color: red"><form:errors path="email"/></td>
		</tr>
		<tr height="50">
			<td>Login </td>
			<td><form:input type="text" path="login"/></td>
			<td style="color: red"><form:errors path="login"/></td>
		</tr>
		<tr height="50">
			<td>Password </td>
			<td><form:input type="password" path="password"/></td>
			<td style="color: red"><form:errors path="password"/></td>
		</tr>
		<tr height="50">
			<td></td>
			<td><input type="button" id="btnReg" value="Register"/></td>
			<td></td>
		</tr>
	</table>	
</form:form>

<script type="text/javascript">
$('#btnReg').click(function () {
	$.post('/blog/register', $('#formReg').serialize(), function (data) {
		$('#formContainer').html(data);
	});
});
</script>