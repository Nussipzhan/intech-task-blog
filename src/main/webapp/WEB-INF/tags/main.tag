<%@ tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ attribute name="title"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>${title}</title>
<spring:url value="/resources/css/main.css" var="mainStyleUrl" />
<link rel="stylesheet" type="text/css" href="${mainStyleUrl}">
<spring:url value="/resources/js/jquery-3.0.0.js" var="jqueryUrl" />
<script type="text/javascript" src="${jqueryUrl}"></script>
</head>
<body>

<div id="leftBlock">
	<%@include file="nav.jsp" %>
</div>

<div id="mainBlock">
	<h2>${title}</h2>
	<jsp:doBody/>
</div>

</body>

</html>
