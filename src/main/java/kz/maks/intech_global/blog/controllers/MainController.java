package kz.maks.intech_global.blog.controllers;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kz.maks.intech_global.blog.dao.CommentDao;
import kz.maks.intech_global.blog.dao.PostDao;
import kz.maks.intech_global.blog.dao.RoleDao;
import kz.maks.intech_global.blog.dao.UserDao;
import kz.maks.intech_global.blog.entities.Comment;
import kz.maks.intech_global.blog.entities.Post;
import kz.maks.intech_global.blog.entities.Role;
import kz.maks.intech_global.blog.entities.User;
import kz.maks.intech_global.blog.models.CreateCommentForm;
import kz.maks.intech_global.blog.models.CreatePostForm;
import kz.maks.intech_global.blog.models.LoginForm;
import kz.maks.intech_global.blog.models.RegForm;
import kz.maks.intech_global.blog.validators.RegFormValidator;

@Controller
public class MainController {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired 
	private PostDao postDao;
	
	@Autowired
	private CommentDao commentDao;
	
	@Autowired
	private RegFormValidator regFormValidator;
	
	private Logger log = Logger.getLogger(MainController.class);
	
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute(new RegForm());
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@Valid RegForm regForm, BindingResult bindingResult, Model model) {
    	regFormValidator.validate(regForm, bindingResult);
    	
    	if (bindingResult.hasErrors()) {
    		model.addAttribute(regForm);
    		return "regForm";
    	} 
    	
    	User user = new User();
    	user.setFirstName(regForm.getFirstName());
    	user.setLastName(regForm.getLastName());
    	user.setMiddleName(regForm.getLastName());
    	user.setEmail(regForm.getEmail());
    	user.setLogin(regForm.getLogin());
    	user.setPassword(regForm.getPassword());
    	{
    		Role roleGuest = roleDao.findOneByName(Role.ROLE_GUEST);
    		user.getRoles().add(roleGuest);
    	}
    	userDao.save(user);
    	
    	return "redirect:/";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error, Model model) {
    	model.addAttribute(new LoginForm());
    	
    	if (error != null) {
    		model.addAttribute("error", "Invalid login or password");
    	}
    	
        return "login";
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
    	List<Post> posts = postDao.findAll();
    	model.addAttribute("posts", posts);
        return "home";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/createPost", method = RequestMethod.GET)
    public String createPost(Model model) {
    	model.addAttribute(new CreatePostForm());
        return "createPost";
    }
    
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/createPost.do", method = RequestMethod.POST)
    public String createPostDo(@Valid CreatePostForm createPostForm, BindingResult bindingResult, Model model, Principal principal) {
    	if (bindingResult.hasErrors()) {
    		model.addAttribute(createPostForm);
    		return "createPostForm";
    	} 
    	
    	Post post = new Post();
    	{
    		String login = principal.getName();
			User user = userDao.findOneByLogin(login);
			post.setAuthor(user);
    	}
    	post.setTitle(createPostForm.getTitle());
    	post.setContent(createPostForm.getContent());
    	postDao.save(post);
    	
        return "redirect:/viewPost/" + post.getId();
    }
    
    @Transactional
    @RequestMapping(value = "/viewPost/{postId}", method = RequestMethod.GET)
    public String viewPost(@PathVariable Long postId, Model model) {
    	Post post = postDao.findOne(postId);
    	log.debug("post = " + post.getTitle());
    	model.addAttribute(post);
    	model.addAttribute(new CreateCommentForm());
        return "viewPost";
    }
    
    @Secured({"ROLE_ADMIN", "ROLE_GUEST"})
    @RequestMapping(value = "/createComment/{postId}", method = RequestMethod.POST)
    public String createComment(@PathVariable Long postId, @Valid CreateCommentForm createCommentForm, 
    		BindingResult bindingResult, Principal principal, Model model) {
    	if (bindingResult.hasErrors()) {
    		model.addAttribute(createCommentForm);
    		return "createCommentForm";
    	}
    	
    	Post post = postDao.findOne(postId);
    	Comment comment = new Comment();
    	comment.setPost(post);
    	{
    		String login = principal.getName();
    		User user = userDao.findOneByLogin(login);
        	comment.setAuthor(user);
    	}
    	comment.setContent(createCommentForm.getContent());
    	commentDao.save(comment);
    	model.addAttribute(comment);
    	return "commentBlock"; 
    }

}
