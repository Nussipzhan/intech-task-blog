package kz.maks.intech_global.blog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kz.maks.intech_global.blog.dao.UserDao;
import kz.maks.intech_global.blog.entities.Role;
import kz.maks.intech_global.blog.entities.User;

@Service
@Transactional(readOnly = true)
public class SecurityService implements UserDetailsService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = userDao.findOneByLogin(login);
		
		if (user == null) {
			throw new UsernameNotFoundException(login + " not found");
		}
		
		List<GrantedAuthority> authList = new ArrayList<>();
		List<Role> roles = user.getRoles();
		
		for (Role role : roles) {
			authList.add(new SimpleGrantedAuthority(role.getName()));
		}
		
		return new org.springframework.security.core.userdetails.User(
				user.getLogin(), user.getPassword(), authList);
	}
	
}
