package kz.maks.intech_global.blog.models;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class RegForm {
	@NotEmpty(message = "Please enter your last name")
    private String lastName;
	@NotEmpty(message = "Please enter your first name")
    private String firstName;
	@NotEmpty(message = "Please enter your middle name")
    private String middleName;
	@NotEmpty(message = "Please enter your email")
	@Email
    private String email;
	@NotEmpty(message = "Please enter your login")
	@Size(min = 6, max = 10)
    private String login;
	@NotEmpty(message = "Please enter your password")
	@Size(min = 6, max = 10)
    private String password;

	public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

	public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
	public String toString() {
		return "UserModel [lastName=" + lastName + ", firstName=" + firstName + ", middleName="
				+ middleName + ", email=" + email + ", login=" + login + ", password=" + password + "]";
	}
}
