package kz.maks.intech_global.blog.models;

import org.hibernate.validator.constraints.NotEmpty;

public class CreatePostForm {
	@NotEmpty(message = "Please enter post title")
	private String title;
	@NotEmpty(message = "Please enter post content")
	private String content;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
