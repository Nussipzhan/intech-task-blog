package kz.maks.intech_global.blog.models;

import org.hibernate.validator.constraints.NotEmpty;

public class CreateCommentForm {
	@NotEmpty(message = "Please enter comment text")
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
