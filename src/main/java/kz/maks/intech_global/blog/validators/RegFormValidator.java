package kz.maks.intech_global.blog.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import kz.maks.intech_global.blog.dao.UserDao;
import kz.maks.intech_global.blog.models.RegForm;

@Component
public class RegFormValidator implements Validator {

	@Autowired
	private UserDao userDao;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return RegForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegForm regForm = (RegForm) target;
		
		if (!errors.hasFieldErrors("login") && userDao.existsByLogin(regForm.getLogin())) {
			errors.rejectValue("login", "", "Login is already in use");
		}
		
		if (!errors.hasFieldErrors("email") && userDao.existsByEmail(regForm.getEmail())) {
			errors.rejectValue("email", "", "Email is already in use");
		}
	}

	
	
}
