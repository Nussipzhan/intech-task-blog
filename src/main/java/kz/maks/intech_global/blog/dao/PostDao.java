package kz.maks.intech_global.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kz.maks.intech_global.blog.entities.Post;

@Repository
public interface PostDao extends JpaRepository<Post, Long> {

}
