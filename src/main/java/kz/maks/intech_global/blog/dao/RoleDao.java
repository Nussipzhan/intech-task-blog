package kz.maks.intech_global.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kz.maks.intech_global.blog.entities.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {
	
	Role findOneByName(String name);

}
