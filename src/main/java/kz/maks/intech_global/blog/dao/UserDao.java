package kz.maks.intech_global.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import kz.maks.intech_global.blog.entities.User;

@Repository
public interface UserDao extends JpaRepository<User, Long> {
	
	@Query("select count(e) > 0 from User e where e.login = ?1")
	boolean existsByLogin(String login);
	
	@Query("select count(e) > 0 from User e where e.email = ?1")
	boolean existsByEmail(String email);
	
	User findOneByLogin(String login);
	
}
